package de.codese.tests.grpctest0;

import de.codese.config.*;

public interface TestServerConfig {

	public static final String DEFAULT_CONFIG_FILE_PATH_STRING_KEY = "testserver.confpath";
	public static final String DEFAULT_CONFIG_FILE_PATH_STRING = "conf/config.properties";

	@Option(documentation = "the path of the pem-encoded file containing the certificate-chain",
			defaultEncodedValue = "cert/certchain.pem")
	public String getCertificateChainPath();

	@Option(documentation = "the path pf the PK8-encoded file containing the server's private key",
			defaultEncodedValue = "cert/privatekey.pk8")
	public String getPrivateKeyPath();

	@Option(documentation = "the port the server should listen to",
			defaultEncodedValue = "34376")
	public int getBindPort();

	public static TestServerConfig getConfig() {
		return ConfigManager.getPropertiesConfig( //
				TestServerConfig.class, //
				DEFAULT_CONFIG_FILE_PATH_STRING_KEY, //
				DEFAULT_CONFIG_FILE_PATH_STRING);
	}
}
