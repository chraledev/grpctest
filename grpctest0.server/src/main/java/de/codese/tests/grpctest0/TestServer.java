package de.codese.tests.grpctest0;

import java.io.*;
import java.nio.charset.*;
import java.util.*;
import java.util.concurrent.*;

import org.soulwing.rot13.*;

import io.grpc.*;
import io.grpc.stub.*;

public class TestServer {

	public static enum TestCodec {

		IDENTITY, //
		ROT13 {

			@Override
			public String encode(final CharSequence src) {
				return Rot13.rotate(src);
			}
		},
		BASE64 { //

			@Override
			public String encode(final CharSequence src) {
				try {
					return new String(Base64.getEncoder().encode(src.toString().getBytes(StandardCharsets.UTF_8)),
							StandardCharsets.US_ASCII);
				}
				catch (final Exception e) {
					return e.getMessage();
				}
			}
		};

		public String encode(final CharSequence src) {
			return src.toString();
		}

		public static TestCodec getRandomCodec(final Random random) {
			final TestCodec[] v = values();
			return v[random.nextInt(v.length)];
		}

	}

	public static class TestServiceImpl extends TestServiceGrpc.TestServiceImplBase {

		private final Random random = new Random();

		@Override
		public void testMe(final RequestData request, final StreamObserver<ResponseData> responseObserver) {
			final TestCodec codec = TestCodec.getRandomCodec(random);
			final String text = request.getTextInput();
			final String misc = request.getMiscData0();
			final ResponseData resp = ResponseData.newBuilder() //
					.setTextInput(text) //
					.setTextOutput(codec.encode(text) + " (" + misc + ")") //
					.setCodecUsed(codec.name()) //
					.build();
			responseObserver.onNext(resp);
			responseObserver.onCompleted();
		}

	}

	public static final void main(final String[] arguments) throws Exception {

		final TestServerConfig config = TestServerConfig.getConfig();

		final ServerBuilder< ? > bld = ServerBuilder.forPort(config.getBindPort());
		final File certChain = new File(config.getCertificateChainPath());
		final File privkey = new File(config.getPrivateKeyPath());

		final Executor executor = Executors.newCachedThreadPool();
		bld//
				.executor(executor)//
				.useTransportSecurity(certChain, privkey)//
				.addService(new TestServiceImpl());
		final Server server = bld.build();
		server.start();
		server.awaitTermination();
	}

}
