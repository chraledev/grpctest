﻿using System;
using System.IO;
using System.Threading.Tasks;
using Grpc.Core;
using Gtk;
using grpctest0.proto;

#pragma warning disable RECS0021 // Warns about calls to virtual member functions occuring in the constructor
public partial class MainWindow : Gtk.Window {

	private Channel channel = null;
	private TestService.TestServiceClient client = null;

	public MainWindow() : base(Gtk.WindowType.Toplevel) {
		Build();
		entry.Activated += OnEntryEnter;
		connectbutton.Clicked += OnConnectClicked;
	}

	private void setConnected(bool connected) {
		entry.Sensitive = connected;
		connectbutton.Sensitive = !connected;
	}

	protected async void OnDeleteEvent(object sender, DeleteEventArgs a) {
		if (channel != null) {
			await channel.ShutdownAsync();
		}
		Application.Quit();
		a.RetVal = true;
	}

	private async void OnEntryEnter(object sender, EventArgs e) {
		if (client == null) {
			textview.Buffer.Text = "not connected";
			setConnected(false);
		}
		else {
			RequestData req = new RequestData();
			req.TextInput = entry.Text;
			req.MiscData0 = System.Environment.UserName;
			textview.Buffer.InsertAtCursor("> " + entry.Text + "\n");
			entry.Text = "";
			ResponseData resp = await client.testMeAsync(req);
			if (resp == null) {
				textview.Buffer.InsertAtCursor(">> RESPONSE <null>\n");
			}
			else {
				textview.Buffer.InsertAtCursor(
					">> '" + resp.TextInput + "' -> '" +
					 resp.TextOutput + "' (" + resp.CodecUsed + ")\n");
			}
		}
	}

	private async void OnConnectClicked(object sender, EventArgs e) {
		this.client = await connect();
		if (this.client != null) {
			textview.Buffer.Text = "connected\n";
			setConnected(true);
			entry.GrabFocus();
		}
	}

	private async Task<TestService.TestServiceClient> connect() {
		channel = createChannel();
		await channel.ConnectAsync();
		return new TestService.TestServiceClient(channel);
	}

	private static Channel createChannel() {
		SslCredentials cred = new SslCredentials(File.ReadAllText("../../../grpctest0.server/cert/certchain.pem"));
		return new Channel("localhost", 34376, cred);
	}

}
