@rem Generate the C# code for .proto files
@echo off

setlocal

@rem enter this directory
cd /d %~dp0

set TOOLS_PATH=packages\Grpc.Tools.1.0.1\tools\windows_x64

%TOOLS_PATH%\protoc.exe --csharp_out grpctest.proto  grpctest0.server/src/main/proto/grpctest0.proto --grpc_out grpctest.proto --plugin=protoc-gen-grpc=%TOOLS_PATH%\grpc_csharp_plugin.exe --csharp_opt=file_extension=.g.cs

endlocal
